var express = require('express');
var app = express();

var port = 4500;

app.use(express.static(__dirname));

app.get("/",function(req,res){
  res.render("index.html");
})

app.listen(port, function() {
  console.log("app running");
})
