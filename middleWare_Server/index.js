const  request = require('request-promise-native');


const rocketChatServer = 'https://dalecoinchat.com:4000';


module.exports = {

     loginUser : async function  (username, password) {

      console.log("calling log in user at index js");

      const response = await request({
        url: `${rocketChatServer}/api/v1/login`,
        method: 'POST',
        json: {
          "user" : username,
          "password" : password
        }
      });
      console.log("response in index js",response);
      return response;
      
      },

      logoutUser : async function  (authToken, userId) {
        const response = await request({
          url: `${rocketChatServer}/api/v1/logout`,
          method: 'POST',
          headers: {
            'X-Auth-Token': authToken,
            'X-User-Id': userId
          }
        });
        console.log("this is response",response);
        return response;
      },

      createUser : async function (username, name, email, password, adminAuthToken, adminUserId) {

        const rocketChatUser = await request({
          url: `${rocketChatServer}/api/v1/users.create`,
          method: 'POST',
          json: {
            name,
            email,
            password,
            username,
            verified: true
          },
          headers: {
            'X-Auth-Token': adminAuthToken,
            'X-User-Id': adminUserId
          }
        });
        return rocketChatUser;
      },

      adminLogin : async function() {
        const response = await request({
          url: `${rocketChatServer}/api/v1/login`,
          method: 'POST',
          json: {
            user: "adminuser",
            password: "adminuser"
          }
        });
        return response;
      },

      listGroups : async function(authToken,userId) {
        const response = await request({
          url: `${rocketChatServer}/api/v1/rooms.get`,
          method: 'GET',
          headers: {
            'X-Auth-Token': authToken,
            'X-User-Id': userId
          }
        });
        return response;
      },

      getUserId : async function(name,authUserId,authToken) {
        var headers = {
          'X-Auth-Token': authToken,
          'X-User-Id': authUserId
      };
      
      var options = {
          url: `${rocketChatServer}/api/v1/users.info?username=`+name,
          headers: headers
      };
      
      const response = await request(options);
      return response;
      },

      activateUser : async function (idToActivate,authuserId,authToken){

        const response = await request({
          url: `${rocketChatServer}/api/v1/users.update`,
          method: 'POST',
          json: {
            userId: idToActivate,
            data: { active: true }
          },
          headers: {
            'X-Auth-Token': authToken,
            'X-User-Id': authuserId
          }
        });
        return response;
    
      },

      deactivateUser : async function (idToActivate,authuserId,authToken){

        const response = await request({
          url: `${rocketChatServer}/api/v1/users.update`,
          method: 'POST',
          json: {
            userId: idToActivate,
            data: { active: false }
          },
          headers: {
            'X-Auth-Token': authToken,
            'X-User-Id': authuserId
          }
        });
        return response;
    
      },

      createGroup : async function (channelName,authuserId,authToken){

        const response = await request({
          url: `${rocketChatServer}/api/v1/channels.create`,
          method: 'POST',
          json: {
            name: channelName
          },
          headers: {
            'X-Auth-Token': authToken,
            'X-User-Id': authuserId
          }
        });
        return response;
    
      },

      addAllUsers :  async function (channelId,authuserId,authToken){

      const response = await request({
          url: `${rocketChatServer}/api/v1/channels.addAll`,
          method: 'POST',
          json: {
            roomId: channelId
          },
          headers: {
            'X-Auth-Token': authToken,
            'X-User-Id': authuserId
          }
        });

        return response;
    
      },

      getGroupType : async function(groupName, authUserId, authToken) {

        console.log("calling get group Type", groupName,authUserId,authToken);

        var headers = {
          'X-Auth-Token': authToken,
          'X-User-Id': authUserId
        };
      
      var options = {
          url: `${rocketChatServer}/api/v1/channels.info?roomName=`+groupName,
          headers: headers
      };
      
   
      try {

        const response = await request(options);
        return JSON.parse(response).success;

      } catch(e){

        console.log("error in try catch");
        return JSON.parse(e.error).success;

      }
    },


    setDefault : async function(groupId,authUserId,authToken) {

      const response = await request({

        url: `${rocketChatServer}/api/v1/channels.setDefault`,
        method: 'POST',
        json: {
          roomId: groupId,
          default : true
        },
        headers: {
          'X-Auth-Token': authToken,
          'X-User-Id': authUserId
        }
      });

      return response;

    },

    getCount : async function(authUserId,authToken) {
        var headers = {
          'X-Auth-Token': authToken,
          'X-User-Id': authUserId
      };
      
      var options = {
          url: `${rocketChatServer}/api/v1/users.list`,
          headers: headers
      };
      
      const response = await request(options);
      return response;
    }

}





