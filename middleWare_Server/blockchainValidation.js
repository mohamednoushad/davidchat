var ethers = require('ethers');
var Web3 = require('web3');
var web3 = new Web3();
var uuidByString = require('uuid-by-string');
var provider = ethers.getDefaultProvider();
var address  = '0x358CdE7EFd260e4b2873C31AE846c857AA122fDf';
var abi = [{"constant":false,"inputs":[{"name":"_groupName","type":"bytes32"},{"name":"_amountOfTokens","type":"uint256"}],"name":"setGroupStakingRequirement","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_amountOfTokens","type":"uint256"}],"name":"setStakingRequirement","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"constant":true,"inputs":[{"name":"","type":"bytes32"}],"name":"administrators","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"bytes32"}],"name":"groupRequirement","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"stakingRequirement","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_userAddress","type":"address"}],"name":"userTokenBalance","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_userAddress","type":"address"}],"name":"validateUser","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_userAddress","type":"address"},{"name":"_groupName","type":"bytes32"}],"name":"validateUserForGroup","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"}];

var contract = new ethers.Contract(address,abi,provider);

module.exports = {
    checkUserEligibility : async function(account) {
        console.log(account);
        return contract.validateUser(account); 
    },

    getUserBalance: async function(account){
        return contract.userTokenBalance(account);

    },

    requiredTokens : async function() {
        return contract.stakingRequirement();
    },

    checkJoiningGroupEligibility : async function(account,groupName) {
        console.log("user group join eligibilty");
        try{
            let groupNameUuid = uuidByString(groupName);
            let groupNameconverted32 = groupNameUuid.replace(/-/g, "");
            return await contract.validateUserForGroup(account,web3.fromAscii(groupNameconverted32));
        } catch(e) {
            console.log(e);
            return false;
        }
 
    },

    getChatGroupsRequirement : async function(groupName) {
        let groupNameUuid = uuidByString(groupName);
        let groupNameconverted32 = groupNameUuid.replace(/-/g, "");
        return contract.groupRequirement(web3.fromAscii(groupNameconverted32));
    }

    
}


