var express = require('express');
const cors = require('cors')
const interval = require('interval-promise')
//var cookieSession = require('cookie-session');
var session = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
const userFunctions = require('./index');
const validateUser = require('./blockchainValidation');
const db = require('./mysql');

var app = express();


// define middleware
const middleware = [

cookieParser(),
session({
secret: 'super-secret-key',
key: 'super-secret-cookie',
resave: false,
saveUninitialized: true,
cookie: { maxAge: 1209600000 } // two weeks in miliseconds
  })
]
app.use(middleware);


app.use(cors({credentials: true, origin: true}))
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.set('view engine', 'ejs')



app.use(function(req, res, next) {
  var allowedOrigins = ['https://dalecoinchat.com', 'https://dalecoinchat.com:4000', 'http://68.183.125.38:4500'];
  var origin = req.headers.origin;
  if(allowedOrigins.indexOf(origin) > -1){
       res.setHeader('Access-Control-Allow-Origin', origin);
  }

  //res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

let tempSession;
let tempUserId;


let stoppedExternally = false
function stopExternally(){
  stoppedExternally = true
}

//initialising variables for interval search to check if user loses token
let checkUserId;
let checkUserAccount;


const mainChatServer  = "https://dalecoinchat.com:4000";


async function saveUser(name,account) {

  console.log("saving user in db",name,account);
  try {
    
  let tokensWithUserBN = await validateUser.getUserBalance(account);

  let tokensWithUser = convertEightDecimalToToken(tokensWithUserBN.toNumber());

  let userStatus = await db.checkifUserExist(name);

  if (userStatus) {

    await db.updateUserData(name,account,tokensWithUser);

  } else {

    await db.insertUser(name,account,tokensWithUser);

  }

} catch (e) {
  console.log(e);
}

}

function convertEightDecimalToToken (e) {
  return e / 1e8
}



async function checkUserEligibilityToReactivate(account){

  return await validateUser.checkUserEligibility(account);
      
}



function validateuserOnIntervals() {
  
  interval(async () => {
    if (stoppedExternally) {

      stop();

     }else{

      let condition = await checkUserEligibilityToReactivate(checkUserAccount);

        if(!condition){
          console.log("user is not eligible");
          //user not eligible
          // var adminCreds = await userFunctions.adminLogin();
          // deActivate = await userFunctions.deactivateUser(checkUserId,adminCreds.data.userId,adminCreds.data.authToken);

          // if(deActivate.status){
          // stopExternally();
          // stop();
          //     }else{
          //     stop();
          // }
        }else{
        console.log("user eligible, continue");
        }

      }
   
  }, 30000, {iterations: 10})
}



app.get('/registration',function(req,res){

    res.render('registration');

 });


app.post('/api/v1/validateGroupJoining',async function(req,res){

  let accountAddress = req.body.userAccount;
  console.log("logging user address",accountAddress);
  let groupToJoin = req.body.groupName;
  console.log("group to join",groupToJoin);

  try {

    const adminCreds = await userFunctions.adminLogin();
    console.log(adminCreds);
    const groupStatus = await userFunctions.getGroupType(groupToJoin,adminCreds.data.userId,adminCreds.data.authToken);
    console.log("here",groupStatus);
  
    if(groupStatus){

      validateUser.checkJoiningGroupEligibility(accountAddress,groupToJoin)
    .then(function(response){
      if(response){
        res.send({valid : response});
      }else{

        validateUser.getChatGroupsRequirement(groupToJoin)
        .then(function(result){
          validateUser.getUserBalance(accountAddress)
          .then(function(userTokens){
            res.send({chatRequirement:convertEightDecimalToToken(result),userBalance : convertEightDecimalToToken(userTokens)})
          })
          .catch(function(error){
            console.log(error);
            res.send(error);
          })
         })
        .catch(function(error){
          console.log(error);
          res.send(error);
        })
      }
   })
    .catch(function(error){
      console.log(error);
      console.log("user account does not hold tokens");
      res.send("user-has-no-tokens");
    })

  }else {

    console.log(groupStatus);
    console.log("its a private or un acccessible group");
    res.send("error-room-not-found");

}
    
  }catch(e){
    console.log("error is here",e);
    res.send(e);
  }

});

app.post('/api/v1/validateUser',function(req,res){

  let account = req.body.userAccount;

  try {

      validateUser.checkUserEligibility(account)
      .then(function(result){
        if(result){
          validateuserOnIntervals();
          res.send({status:result});
        }else{
          validateUser.getUserBalance(account)
          .then(function(userTokens){
            validateUser.requiredTokens()
            .then(function(stakeRequirement){
              console.log(userTokens,stakeRequirement);
              res.send({status : result, userBalance : convertEightDecimalToToken(userTokens), stakeRequirement: convertEightDecimalToToken(stakeRequirement)});

            })
          })
        }
      })
      .catch(function(error){
        res.send(error);
      });

  }catch(e){
    res.send(e);
  }

});

app.post('/api/v1/createGroup',async function(req,res){

  let groupName = req.body.channelName;

  try {

    const adminCreds = await userFunctions.adminLogin();
    const addGroup = await userFunctions.createGroup(groupName,adminCreds.data.userId,adminCreds.data.authToken);

    if (addGroup.success){

      const addUsers = await userFunctions.addAllUsers(addGroup.channel._id,adminCreds.data.userId,adminCreds.data.authToken);
      if(addUsers.success){

        const groupDefaultStatus = await userFunctions.setDefault(addGroup.channel._id,adminCreds.data.userId,adminCreds.data.authToken);

        if(groupDefaultStatus.success) {

          res.send("success");
        } else {

          console.log("group making default was unsuccessful");

          res.send("group making default was unsuccessful");
        }

        
      }else{
        console.log("adding users to group unsuccessful");
        res.send("adding users to group unsuccessful");
      }
    }else{
      console.log("creating group unsuccessful");
    }
  
  }catch(e){
    console.log(e);
    res.send(e);
  }

});

app.get('/login',function(req,res){

  res.render('index');

});

app.get('/',function(req,res){

  sess = req.session;

  console.log(sess);

  if(sess.rocketchatAuthToken) {

    res.render('main');

  } else {

    res.render('home');

  }

});

app.post('/api/v1/login',async function(req,res){
    
  let user = req.body;
  console.log("calling login function");
  console.log(user);
    
  try{
     
      userFunctions.loginUser(user.username,user.password)
      .then(function(logIn){

        console.log("response from login user",logIn);

        if(logIn && logIn.status == "success") {

          //function to saveinDb
          saveUser(user.username,user.userAccount);

          checkUserId = logIn.data.userId;
          checkUserAccount = user.userAccount;
          res.set('Content-Type', 'text/html');
          req.session.user = user; 
          req.session.rocketchatAuthToken = logIn.data.authToken;
          tempSession = logIn.data.authToken;
          tempUserId = logIn.data.userId;
          req.session.rocketchatUserId = logIn.data.userId;
          req.session.checkUserId = logIn.data.userId;
          req.session.checkAccountAddress = user.userAccount;
          res.send(`<script>
          window.parent.postMessage({
            event: 'login-with-token',
            loginToken: '${ logIn.data.authToken }'
          }, mainChatServer); // rocket.chat's URL
          </script>`);
      
        }else {
          console.log(logIn);
          res.send(logIn);
        }
     })
      .catch(function(error){

        console.log(error);

        if (error.response && error.response.body.message=='User is not activated' ){

          checkUserEligibilityToReactivate(user.userAccount)
          .then(function(result){
            if(result){
              userFunctions.adminLogin()
              .then(function(adminDetails){
                userFunctions.getUserId(user.username,adminDetails.data.userId,adminDetails.data.authToken)
                .then(function(userInfo){
                  let idToActivate = JSON.parse(userInfo).user._id;
                  userFunctions.activateUser(idToActivate,adminDetails.data.userId,adminDetails.data.authToken)
                  .then(async function(activeStatus){
                    if(activeStatus.success){
                      checkUserId = activeStatus.user._id;
                      checkUserAccount = user.userAccount;
                      const reloginUser = await userFunctions.loginUser(user.username,user.password);
                      res.set('Content-Type', 'text/html');
                      req.session = user;
                      req.session.rocketchatAuthToken = reloginUser.data.authToken;
                      req.session.rocketchatUserId = reloginUser.data.userId;
                      res.send(`<script>
                      window.parent.postMessage({
                        event: 'login-with-token',
                        loginToken: '${ reloginUser.data.authToken }'
                      }, mainChatServer); // rocket.chat's URL
                      </script>`);
                    }
                  })
                })
             });
            }
          })
        }else{
          console.log(error);
          res.send(error);
        }
    });
  
   }catch(e){
     console.log("outer loop error",e);
     res.send("Oops, Try Again");
 }

});



app.post('/api/v1/createUser',function(req,res){
    
  let user = req.body;

  try{

        userFunctions.adminLogin()
        .then(function(adminDetails){
          userFunctions.createUser(user.username, user.name, user.email, user.password,adminDetails.data.authToken,adminDetails.data.userId)
          .then(function(createdUser){
            if(createdUser.success) {
              res.send("user created successfully");
            }
          })
          .catch(function(error){
            console.log(error);
            res.send("Error Creating User");
          })
        })
      .catch(function(err){
        console.log(err);
        res.send("Admin Authentication Unsuccessful");
      })

  }catch(e){

    console.log("outer loop error",e);
    res.send("Oops, Try Again");

  }

});


// This method will be called by Rocket.chat to fetch the login token
app.get('/api/v1/rocket_chat_auth_get', (req, res) => {

  console.log("inside rocket chat auth get",req.session);
  console.log("rocketchatAuthToken auth from session",req.session.rocketchatAuthToken);
  console.log("inside rocket chat auth get",tempSession);

  if (req.session.rocketchatAuthToken) {
    res.send({ loginToken: req.session.rocketchatAuthToken })
    return;
  } else {
    res.status(401).json({ message: 'User not logged in'});
    return;
  }

});

// This method will be called by Rocket.chat to fetch the login token
// and is used as a fallback
app.get('/api/v1/rocket_chat_iframe', (req, res) => {

  console.log("inside rocket chat iframe",req.session);
  console.log("rocketchatAuthToken iframe from session",req.session.rocketchatAuthToken);

  console.log("inside rocket chat iframe temp session",tempSession);

  const rocketChatServer = mainChatServer;
  if (req.session.rocketchatAuthToken) {
    return res.send(`<script>
      window.parent.postMessage({
        event: 'login-with-token',
        loginToken: '${ req.session.rocketchatAuthToken}'
      }, '${ rocketChatServer }');
    </script>
    `)
    return;
  } else {
    return res.status(401).send('User not logged in')
  }
 
})



app.get('/logout',function(req,res){

  res.render('index');

});


app.get('/chat',function(req,res){

  console.log("calling chat");

  sess = req.session;

  console.log(sess);

  if(tempSession) {

    res.render('main');

  } else {
    res.render('index');
  }

 
});

app.get('/api/v1/connectUrl',function(req,res){

  console.log("calling connect url");

  let returnUrl = mainChatServer+"/channel/";

  res.send(returnUrl);

});

app.get('/api/v1/logout',function(req,res){

  console.log("calling logout function");
  console.log("auth token",tempSession);
  console.log("authuser id",tempUserId);

  userFunctions.logoutUser(req.session.rocketchatAuthToken, req.session.rocketchatUserId)
  .then(function(result){
    console.log("with result back in main server page",result);
    //added here
    tempSession = null;
    tempUserId = null;
    req.session = null;
    res.clearCookie("super-secret-cookie");
    res.clearCookie("connect.sid");
    res.clearCookie("rc_token");
    res.clearCookie("rc_uid");
    res.clearCookie("session.sig");
    res.clearCookie("session");
    res.send('logged out successfully');
  })
  .catch(function(error){
  console.log("error logging out user",error);
  res.send(error);
})

});


app.get('/api/v1/listGroups',function(req,res){

  console.log(req.session.rocketchatAuthToken);
  console.log(req.session.rocketchatUserId);

  try{
    userFunctions.listGroups(req.session.rocketchatAuthToken,req.session.rocketchatUserId)
    .then(function(groupList){
      list = JSON.parse(groupList);
      if(list.success){
        res.send(list.update);
      } else {
        res.send("cannot get list of groups");
      }
    })
    .catch(function(error){
      console.log(error);
      res.send(error);
    })

  } catch(e){
    console.log(e);
    res.send(e);
  }

});

app.get('/api/v1/users',async function(req,res){

  console.log("calling all users");

  try {

    let users = await db.getAllUsers();
    res.send(users);


  } catch(e) {
    console.log(e);
    res.send(e);
  }

  
});

app.get('/api/v1/users/:username',async function(req,res){

  console.log("calling individual user");
  console.log(req.params.username);

  try {

    let userDetail = await db.getUser(req.params.username);
    res.send(userDetail);


  } catch(e) {
    console.log(e);
    res.send(e);
  }

  
});

app.get('/api/v1/usercount',async function(req,res){

  console.log("calling get user count");

  try {

    let adminCreds = await userFunctions.adminLogin();
    let usersinfo = await userFunctions.getCount(adminCreds.data.userId,adminCreds.data.authToken);
    let totalUsers = JSON.parse(usersinfo).count;

    res.status(200).send({count:totalUsers});

  } catch(e) {

    console.log(e);
    res.send(e);

  }

})



app.listen(8080,function(){
console.log("App Started on PORT 8080");
});

