
let account = null;
let middleWareUrl = "https://dalecoinchat.com/api/v1";




function validate (input) {
    if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
        if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
            return false;
        }
    }
    else {
        if($(input).val().trim() == ''){
            return false;
        }
    }
}

function showValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).addClass('alert-validate');
}

function hideValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).removeClass('alert-validate');
}





$(document).ready(function(){ 


    $( "#login" ).click(function(event) {

        event.preventDefault();
        event.stopImmediatePropagation();
    
        $(thisAlert).removeClass('alert-login');
        $(thisAlert).removeClass('alert-metamask');
        $(thisAlert).removeClass('alert-credentials');
        $(thisAlert).removeClass('alert-error');
    
    
        let username = $("#username").val();
        let password = $("#password").val();
    
     
        var input = $('.validate-input .input100');

     
    
        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);    
                          //no input
                }else{
              
                var thisAlert = $(input).parent(); 
            

                $(thisAlert).removeClass('alert-validate');   //input present and checking if both username and password is admin
                }

            if(username && password) {
           

                  if(account === null) {

                    $(thisAlert).addClass('alert-metamask');
    
                } else {
    
                    $(thisAlert).removeClass('alert-login');
                    $(thisAlert).removeClass('alert-metamask');
                    $(thisAlert).removeClass('alert-credentials');
                    $(thisAlert).removeClass('alert-error');
    
                    let url = middleWareUrl+'/login';

                  
                    let data = {
                        username : username,
                        password : password,
                        userAccount: account
                    }

                    $.ajaxSetup({
                        crossDomain: true,
                        xhrFields: {
                            withCredentials: true
                        }
                    });
    
                    $.post(url,data,function(response) {
    
                                 
                        if(typeof(response)==="string"){
            
                            window.location.href="/chat";
            
                            } else if (response.error.error=="Unauthorized") {
    
                                $(thisAlert).addClass('alert-credentials');
            
          
                        
                        }else{
    
                            console.log(JSON.stringify(response.error.message));
    
                            $(thisAlert).addClass('alert-error');
           
  
                        }
            
                    });
    
                }
          
                break;
                
            } else if (username && !password) {

                $(thisAlert).removeClass('alert-credentials');

                showValidate(input[1]);

            } else if (!username && password) {

                $(thisAlert).removeClass('alert-credentials');

                showValidate(input[0]);

            } else if(!username && !password) {
                $(thisAlert).removeClass('alert-credentials');

                showValidate(input[0]);
                showValidate(input[1]);


            }

       }            
 
    });

    if (typeof web3 !== 'undefined') {
        console.warn("Using web3 detected from external source like Metamask")
        window.web3 = new Web3(web3.currentProvider);
    } else {
        console.log("ivide");
        $("#modalHeader1").html("Couldn't get any accounts!");
        $("#modelContent1").html("Make sure your Ethereum client is configured correctly");
        $("#walletsAvailable").html("<p>You can install Ethereum Client for Desktop Browsers from  <a href='https://metamask.io/'>Metamask</a> or for Mobile from <a href='https://trustwallet.com/'>Trust Wallet</a></p>");

        $("#myModal1").modal()
        
        console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");

    }


    web3.eth.getAccounts(function (err, accs) {
      if (err != null) {
        alert('There was an error fetching your accounts.')
        return
      }

      if (accs.length === 0) {

        $("#modalHeader1").html("Couldn't get any accounts!");
        $("#modelContent1").html("Make sure your Ethereum client is configured correctly");
        $("#walletsAvailable").html("<p>You can install Ethereum Client for Desktop Browsers from  <a href='https://metamask.io/'>Metamask</a> Or for Mobile from <a href='https://trustwallet.com/'>Trust Wallet</a></p>");

        $("#myModal1").modal()
        return;
      }

      account = accs[0];


    })

    setInterval(
        () => console.clear(),
        1000
      );


  
 });