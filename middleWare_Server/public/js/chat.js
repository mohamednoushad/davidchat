let chatGroupRequirement;
let userOwnedBalance;

function logout() {

    let url = middleWareUrl+'/logout';

    $.get(url,function(data){

        if(data=='logged out successfully'){
            window.location.href = "/";
        }
        
    });
}


function callChatRoom(_this) {

    let groupToJoin = $(_this).attr("value");


            let groupEligibilityUrl = middleWareUrl+'/validateGroupJoining';

            try {

                $.post(groupEligibilityUrl,{userAccount : account, groupName : groupToJoin},function(result){

       
    
                    if(result == "error-room-not-found") {
    
                        $('#modal2').modal('open');
    
                    }else if(result.chatRequirement>0) {
    
                        $('#userNotEligibleError').empty();
                        $('#tokenExchanges').empty();
    
                        chatGroupRequirement = result.chatRequirement;
                        userOwnedBalance = result.userBalance;
    
                        $('#userNotEligibleError').html("You need "+chatGroupRequirement+" Dale Coins to join the chat while you have only "+ userOwnedBalance + " Dale Coins");
                        $('#tokenExchanges').html("<p>You can buy Tokens from: <a href='https://www.mercatox.com/exchange/DALC/BTC'>Mercatox</a> Or <a href='https://www.coinexchange.io/market/DALC/BTC'>CoinExchange</a> Or <a href='https://yobit.net/en/trade/DALC/BTC'>YOBIT</a></p>");
                        $('#modal3').modal('open');
    
                    }else if (result.valid || result.chatRequirement == 0) {
    
                        $("#welcomeMsg1").hide();
                        $("#welcomeMsg2").hide();
                        $("#countDisplay").hide();
   
                        $("#groupname").html(groupToJoin.toUpperCase());

                        $.ajaxSetup({
                            crossDomain: true,
                            xhrFields: {
                                withCredentials: true
                            }
                        });

                        $.get(middleWareUrl+'/connectUrl',function(resultUrl) { 
    
                        let iframeUrl = resultUrl+groupToJoin+"?layout=embedded";

                        $("#chatWindow").addClass("hide");
                        $(".progress").removeClass("hide");
                        
                        $("#chatWindow").attr("src", iframeUrl);

                        var delayInMilliseconds = 7000; //7 second

                        setTimeout(function() {
                            $(".progress").addClass("hide");
                            $("#chatWindow").removeClass("hide");
                            }, delayInMilliseconds);

                        });
    
                    }else {
                     
                        alert(result);
                    }
    
                });

            } catch(e){

                $("#chatWindow").attr("src", "");
            }

          
 }

 function dynamicSort(property) {
    var sortOrder = 1;

    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }

    return function (a,b) {
        if(sortOrder == -1){
            return b[property].localeCompare(a[property]);
        }else{
            return a[property].localeCompare(b[property]);
        }        
    }
}




$(document).ready(function(){


    $('.modal').modal();

    middleWareUrl = "https://dalecoinchat.com/api/v1";
  
    

    let iconArray = ["forum", "insert_comment", "mms" , "beach_access","book","cached","cast","chat_bubble"];
    let colors = ["teal","red","pink","purple","deep-purple","indigo","blue","light-blue","cyan","green","lime","yellow","amber","brown"];
    let color = "red";

    $.ajaxSetup({
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        }
    });


    //function to list all groups
    $.get(middleWareUrl+'/listGroups',function(data){



        data.sort(dynamicSort("name"))


            $.each(data, function(index, value) {

                if(value.name) {

                    icon = iconArray[Math.floor(Math.random() * iconArray.length)];
                    color = colors[Math.floor(Math.random() * colors.length)];
                    
     
                    $(".collection").append('<li class="collection-item avatar chatroom"><i class="material-icons chatGroupList circle"'+color+'">'+icon+'</i><span class="title chatroom" id="'+value.name+'" value="'+value.name+'" onclick="callChatRoom(this)">'+(value.name).toUpperCase()+'</span></li>');

                    $(".chatGroupList").toggleClass(color);
     
                }
            });
    });

    if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source like Metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
        } else {
            console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
            // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
            window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
            
        }


    web3.eth.getAccounts(function (err, accs) {
      if (err != null) {
        alert('There was an error fetching your accounts.')
        return
      }

      if (accs.length === 0) {
        $('#modal1').modal('open');
        return
      }

      account = accs[0]

    })

    

      setInterval(
        () => console.clear(),
        1000
      );

      $.ajaxSetup({crossDomain: true,xhrFields: {withCredentials: true}});
  
      $.get('https://dalecoinchat.com/api/v1/usercount',function(result) { 

          $("#usersmaincount").html(result.count);

      });
  
 });
